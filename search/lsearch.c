#include "lsearch.h"

int lsearch(int element, int *array, int size){
	int i;
	for(i=0;i<size;i++){
		if(array[i]==element){
			return i;
		}
	}
	return -1;
}

void* g_lsearch(void *element,void *array,int elem_size,int size,int(*comp)(void*,void*)){
	int i;
	
	void *cur;

	for(i=0;i<size;i++){
		cur=(char*)array+i*elem_size;
		if(comp(cur,element)){
			return cur;
		}
	}
	return 0;
}