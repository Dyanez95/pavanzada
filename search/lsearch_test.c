#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "lsearch.h"

void test_lsearch(void);
void test_g_lsearch(void);
int cmp_char(void*,void*);
int cmp_str(void*,void*);
void test_g_strings_lsearch(void);

int main(void){

	//test_lsearch();

	//test_g_lsearch();

	test_g_strings_lsearch();

	return 0;
}

void test_lsearch(void){

	int a[5]={1,3,90,-1,78};

	int res=lsearch(-1,a,5);

	if(res>=0){
		printf("found %d at %d\n", -1,res);
	}
	else{
		printf("not found\n");
	}
}

void test_g_lsearch(void){

	char a[5]={'b','z','d','f','g'};
	char elem='d';

	char *res=(char*)g_lsearch((void*)&elem,(void*)a,sizeof(char),strlen(a),cmp_char);

	if(res!=0){
		printf("found %c in direction %p\n", *res,res);
	}
	else{
		printf("not found\n");
	}
}

void test_g_strings_lsearch(void){
	char **strings=(char**)malloc(3*sizeof(char*));
	strings[0]=strdup("oh snap");
	strings[1]=strdup("hola");
	strings[2]=strdup("quiero esta cadena");

	char *busqueda=strdup("hola3");

	//char **res=(char**)g_lsearch((void*)(&busqueda),(void*)(&strings),sizeof(char*),3,cmp_str);

	char **r=(char**)g_lsearch((void*)busqueda,(void*)strings,sizeof(char*),3,cmp_str);

	if(r!=0){
		printf("%s encontrado en direccion %p\n", *r, *r);
	}
	else{
		printf("not found\n");
	}
	free(busqueda);
	free(strings[0]);
	free(strings[1]);
	free(strings[2]);
	free(strings);
}

int cmp_char(void *a, void *b){
	char va=*((char*)a);
	char vb=*((char*)b);
	return va==vb;
}

int cmp_str(void *p1, void *p2){
	char *str1=*((char**)p1);
	char *str2=(char*)p2;
	int res=strcmp(str1,str2);
	if(res==0){
		return 1;
	}
	return 0;
}