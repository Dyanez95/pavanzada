/*

Problem: http://acm.tju.edu.cn/toj/showp1077.html
Author: Diego Yanez Llamas

*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


int debug=0;

/*
	Solve problem described in the link above

	repeater is an adjacency matrix used to solve
	the problem described in the link above

	size is the length of the array of int*
	repeaters[i] has a lenght of 27

	i={1,...,size+1} i represents the ith repeater, repeaters[i] is the adjacent repeaters to it, it is 1 if actually adjacent or zero otherwise


*/
void channels(int size, int **repeaters){

	if(debug){
		int i,j;

		printf("printing adjacency matrix\n");
		for(i=1;i<(size+1);i++){
			for(j=1;j<27;j++){
				printf("[%d] ", repeaters[i][j]);
			}
			printf("\n");
		}
		printf("\n");
	}

	//used to flag if a repeater has been given a channel
	int *channels=(int*)calloc(27,sizeof(int));

	//we always use a channel at least
	int num_channels=1;

	//first repeater gets a channel right away
	channels[1]=1;

	int i,j,k,adj;

	//iterate over repeaters and their adjacencies 
	for(i=1;i<(size+1);i++){
		for(j=1;j<27;j++){
			//current adjacency
			adj=repeaters[i][j];

			//there's actually an adjacency
			if(adj>0){

				//if the current adjacency has a channel asigned already, do nothing
				if(channels[j]==0){

					//this hardcodes the adjacencies of the first repeater
					//as they always require new channels
					if(i==1){

						channels[j]=1;
						num_channels++;

					}
					//all other cases
					else{

						//want to know if current adjacency is adjacent to some other repeater 
						//other than the current one
						int isAdjacent=0;

						//check all repeaters except the current one
						for(k=1;k<i;k++){
							//check if the current adjacency is also adjacent to k repeater
							if(repeaters[k][j]>0){
								isAdjacent=1;
								break;
							}
						}

						//current repeater gets a channel
						channels[j]=1;

						//if the current adjacency is adjacent to some other repeater other than the current one
						//it needs a new channel
						//otherwise it could use one of the previously assigned channels
						//problem only requires the number of channels not how they should be mapped to repeaters
						if(isAdjacent){
							num_channels++;
						}
					}
				}
			}
		}
	}

	//print solution with correct format
	if(num_channels==1){
		printf("%d channel required\n", num_channels);
	}
	else{
		printf("%d channels required\n", num_channels);
	}
	
}

void read_input(char *filename){

	FILE *f=fopen(filename,"r");

	assert(f!=NULL && "opening file failed");

	char *number_of_repeaters;

	char cur_char;

	while(1){

		cur_char=fgetc(f);

		int i=0;

		//maximum number of repeaters is 26, therefore this array is good enough to hold the number of repeaters in
		//string form
		number_of_repeaters=(char*)calloc(3,sizeof(char));

		while(cur_char!='\n'){
			number_of_repeaters[i]=cur_char;
			i++;
			cur_char=fgetc(f);
		}

		int size=atoi(number_of_repeaters);

		//zero indicates end of input file
		if(size==0){
			break;
		}

		i=1;

		int **repeaters=(int**)calloc(size+1,sizeof(int*));

		//construct adjacency table based on input
		while(i<(size+1)){

			repeaters[i]=(int*)calloc(27,sizeof(int));

			cur_char=fgetc(f);

			//skip until : character
			while(cur_char!=':'){
				cur_char=fgetc(f);
			}

			cur_char=fgetc(f);


			while(cur_char!='\n'){
				//repeaters are named in capital letters A...Z
				//transfrom to positions 1..26
				repeaters[i][(int)(cur_char-64)]=1;
				cur_char=fgetc(f);
			}
			i++;
		}



		//call solution on current instance of the problem
		channels(size,repeaters);
		

		//free arrays used to hold problem data
		//ready for next instance
		free(number_of_repeaters);

		for(i=1;i<(size+1);i++){
			free(repeaters[i]);
		}
		free(repeaters);
	}
}

int main(int argc , char **argv){

	if(argc==2){
		debug=atoi(argv[1]);
	}
	read_input("test.txt");

	return 0;
}