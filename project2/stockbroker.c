/*

Problem : http://acm.tju.edu.cn/toj/showp1075.html
Author : Diego Yanez

*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/*solves a the problem described in link above, one set of contacts
table is a representation of the relationships between contacts

Rows: enumerated contacts in the inteval [1,num_contacts]
Columns: the time (in minutes) it takes for the contact in the 
corresponding row to transmit its message, zero if the contact can't be reached through that particular
contact. Otherwise, the time is in the interval [1,10]
*/
void stockbroker(int num_contacts, int **table){

	int i,j;

	//hold the maximum time of each contact
	int *tmp=(int*)calloc(num_contacts+1,sizeof(int));

	//array of flags to indicate that a contact cant reach all other contacts
	int *disjoint_contact=(int*)calloc(num_contacts+1,sizeof(int));


	for(i=1;i<=num_contacts;i++){
		int max=-1;
		for(j=1;j<=num_contacts;j++){
			//ignore the relationship between the contact and him/herself
			if(i!=j){
				//the contact i cannot reach contact j
				//mark it in the array of flags
				if(table[i][j]<=0){
					disjoint_contact[i]=1;
					break;
				}
				//valid time
				//update max
				if(table[i][j]>max){
					max=table[i][j];
				}
			}
		}
		tmp[i]=max;
	}

	//asume set of contacts is disjoint
	//easier to check that the opposite
	int is_disjoint=1;

	//check if any contact can reach all other contacts
	for(i=1;i<=num_contacts;i++){
		if(!disjoint_contact[i]){
			is_disjoint=0;
			break;
		}
	}

	if(!is_disjoint){

		//problem states that the greatest possible time is 10 minutes, 11 serves
		//well as a dummy value
		int smallest=11;

		int contact=0;

		//check for the nondisjoint contact with the lowest maximum time
		for(i=1;i<=num_contacts;i++){
			if(tmp[i]<smallest && !disjoint_contact[i]){
				smallest=tmp[i];
				contact=i;
			}
		}

		//print solution
		printf("%d %d\n", contact, smallest);
	}
	else{
		//network of contacts is disjoint
		//problem states this should be informed this way
		printf("disjoint\n");
	}

	//deallocate arrays
	free(tmp);
	free(disjoint_contact);
}

//function that reads an input file for the problem described in the link above
//a single input file may contain more than 1 set of contacts
//call the stockbroker function for each set of contacts
//function stops when a set of contacts is indicated to have zero contacts
void read_file_call_sb(char *filename){
	FILE *f=fopen(filename,"r");

	assert(f!=NULL && "cannot open file");

	char cur_char;

	//stop condition is inside as a break
	for(;;){

		//read how many contacts are in this set
		//problem states the maximum number of contacts is 100
		//3 digits are therefore enough

		char *str_num_con=(char*)calloc(4,sizeof(char));

		cur_char=fgetc(f);

		int i=0;

		//read characters until the end of line
		while(cur_char!='\n'){
			str_num_con[i]=cur_char;
			i++;
			cur_char=fgetc(f);
		}

		int num_contacts=atoi(str_num_con);

		free(str_num_con);

		//STOP CONDITION
		//when a set of contacts has zero or less, we stop
		//as established in the problem
		if(num_contacts<=0){
			break;
		}

		//create table to represent the network of contacts
		int **table=(int**)calloc(num_contacts+1,sizeof(int*));

		for(i=0;i<(num_contacts+1);i++){
			table[i]=(int*)calloc(num_contacts+1,sizeof(int));
		}

		i=0;

		int j;

		//iterate over the next num_contacts-1 lines to obtain
		//to fill table with the necessary information
		//line 0 represents contact 1, line 1 contact 2 and so on
		while(i<num_contacts){

			j=0;

			//read number indicating how many related contacts this contact has
			char *str_related_contacts=(char*)calloc(3,sizeof(char));

			cur_char=fgetc(f);

			//numbers are separated by spaces or line escapes
			while(cur_char!=' ' && cur_char!='\n'){
				str_related_contacts[j]=cur_char;
				j++;
				cur_char=fgetc(f);
			}

			int num_related_contacts=atoi(str_related_contacts);
			free(str_related_contacts);

			//if this contact has no related contacts skip it
			if(num_related_contacts==0){		
				i++;
				continue;
			}

			//continue until the end of line
			while(cur_char!='\n'){

				j=0;

				//contacts and the time it takes to reach them
				//come in pairs, first the number of the contact in the interval [1,num_contacts-1]
				//then the time in minutes, in the interval [1,10]
				cur_char=fgetc(f);

				char *str_contact=(char*)calloc(4,sizeof(char));
				char *str_time=(char*)calloc(3,sizeof(char));

				//number continues until the space character
				while(cur_char!=' '){
					str_contact[j]=cur_char;
					j++;
					cur_char=fgetc(f);
				}

				cur_char=fgetc(f);

				j=0;

				//number continues until end of line or space
				while(cur_char!=' ' && cur_char!='\n'){
					str_time[j]=cur_char;
					j++;
					cur_char=fgetc(f);
				}

				//conver from string to integer
				int contact=atoi(str_contact);
				int contact_time=atoi(str_time);

				//update table
				table[i+1][contact]=contact_time;

				//deallocate strings
				free(str_contact);
				free(str_time);
			}
			//continue to next contact
			i++;
		}
		//read a complete set of contacts
		//call stockbroker to solve this set
		stockbroker(num_contacts,table);

		//deallocate table for next set of contacts
		for(i=0;i<num_contacts+1;i++){
			free(table[i]);
		}
		free(table);
	}

	//close file
	fclose(f);
}
/*
	arguments used:
	argv[1]: name of input file

*/
int main(int argc, char **argv){

	assert(argc==2 && "invalid arguments");

	read_file_call_sb(argv[1]);

	return 0;
}