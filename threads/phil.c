#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <time.h>

#define NUMPHILS 4

#define NUMFORKS 4

sem_t forks[NUMFORKS];

sem_t chairs;

void* live(void *data){

	long tid=(long)data;
	printf("hello from phil %ld\n", tid);
	while(1){
		//THINK
		printf("phil %ld thinking\n", tid);
		sleep(rand()%5);

		sem_wait(&chairs);
		sem_wait(&forks[tid]);
		sem_wait(&forks[(tid+1)%NUMFORKS]);

		//EAT
		printf("phil %ld eating\n", tid);
		sleep(rand()%5);

		sem_post(&forks[tid]);
		sem_post(&forks[(tid+1)%NUMFORKS]);
		sem_post(&chairs);
	}
	pthread_exit(NULL);
}

int main(){
	long i;

	pthread_t threads[NUMPHILS];
	pthread_attr_t attr;

	srand(time(NULL));

	for(i=0;i<NUMFORKS;i++){
		sem_init(&forks[i],0,1);
	}

	sem_init(&chairs,0,NUMFORKS-1);

	pthread_attr_init(&attr);

	pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_JOINABLE);

	for(i=0l;i<NUMPHILS;i++){
		pthread_create(&threads[i],&attr,live,(void*)i);
	}

	for(i=0l;i<NUMPHILS;i++){
		pthread_join(threads[i],NULL);
	}

	pthread_exit(NULL);
	return 0;
}