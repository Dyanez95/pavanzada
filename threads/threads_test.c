#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>

#define NUMTHREADS 100

sem_t mutex;

int counter;

void* printHello(void *data){
	int i;
	long tid=(long)data;
	printf("hello from %ld\n", tid);
	for(i=0;i<10000;i++){
		sem_wait(&mutex);
		counter++;
		sem_post(&mutex);
	}

	pthread_exit(NULL);
	return NULL;
}

int main(){
	long i;

	pthread_t threads[NUMTHREADS];
	pthread_attr_t attr;

	sem_init(&mutex,0,1);

	counter=0;

	pthread_attr_init(&attr);

	pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_JOINABLE);

	for(i=0l;i<NUMTHREADS;i++){
		pthread_create(threads+i,&attr,printHello,(void*)i);
	}

	for(i=0l;i<NUMTHREADS;i++){
		pthread_join(threads[i],NULL);
	}

	printf("counter = %d \n", counter);

	pthread_exit(NULL);
	return 0;
}