/*#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int*getSmallestFraction(int x, int y){
    
    int modX,modY;
    
    int smallest=0;
    
    int i=2;
    
    while(i<=x && i<=y){
        modX=x%i;
        modY=y%i;
        
        if(modX==0 && modY==0){
            smallest=i;
        }
        i++;
    }
    
    if(smallest>0){
        int *result=(int*)calloc(2,sizeof(int));
        result[0]=x/smallest;
        result[1]=y/smallest;
        return result;
    }
    return NULL;
}

char** ReduceFraction(int fractions_size, char** fractions, int* result_size) {
    *result_size=fractions_size;
    
    char **result=(char**)calloc(fractions_size,sizeof(char*));
    
    int i;
    
    for(i=0;i<fractions_size;i++){
        int x=atoi(fractions[i]);
        
        char *line=fractions[i];
        
        while((*line)!='/'){
            line++;
        }
        line++;
        int y=atoi(line);
        
        int *smallest_fractions=getSmallestFraction(x,y);
        
        if(smallest_fractions!=NULL){
         
           char x_prime[5];
           sprintf(x_prime,"%d",smallest_fractions[0]);
           
           char y_prime[5];
           sprintf(y_prime,"%d",smallest_fractions[1]);
            
           char holder[10]={'\0'};
            
           strcat(holder,x_prime);
           
           strcat(holder,"/");
           
           strcat(holder,y_prime);
            
           result[i]=strdup(holder);
            
        }
        else{
            result[i]=strdup(fractions[i]);
        }
        free(smallest_fractions);
    }
    return result;
}*/

int*getSmallestFraction(int x, int y){
    
    int modX,modY;
    
    int smallest=0;
    
    int i=2;
    
    while(i<=x && i<=y){
        modX=x%i;
        modY=y%i;
        
        if(modX==0 && modY==0){
            smallest=i;
        }
        i++;
    }
    
    if(smallest>0){
        int *result=(int*)calloc(2,sizeof(int));
        result[0]=x/smallest;
        result[1]=y/smallest;
        return result;
    }
    return NULL;
}

char** ReduceFraction(int fractions_size, char** fractions, int* result_size) {
    *result_size=fractions_size;
    
    char **result=(char**)calloc(fractions_size,sizeof(char*));
    
    int i;
    
    for(i=0;i<fractions_size;i++){
        int x=atoi(fractions[i]);
        
        char *line=fractions[i];
        
        while((*line)!='/'){
            line++;
        }
        line++;
        int y=atoi(line);
        
        int *smallest_fractions=getSmallestFraction(x,y);
        
        if(smallest_fractions!=NULL){
         
           char x_prime[5];
           sprintf(x_prime,"%d",smallest_fractions[0]);
           
           char y_prime[5];
           sprintf(y_prime,"%d",smallest_fractions[1]);
            
           char holder[10]={'\0'};
            
           strcat(holder,x_prime);
           
           strcat(holder,"/");
           
           strcat(holder,y_prime);
            
           result[i]=strdup(holder);
            
        }
        else{
            result[i]=strdup(fractions[i]);
        }
        free(smallest_fractions);
    }
    return result;
}
