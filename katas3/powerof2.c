#include <stdio.h>
#include <stdlib.h>

int isPowerOfTwo(unsigned int x){
	while((x & 1)==0 && x>1){
		x>>=1;
	}
	return (x==1);
}

int* countTwos(int arr_size, int* arr, int* result_size) {

    int *flags=(int*)calloc(arr_size,sizeof(int));
    *result_size=arr_size;
    
    int i;
    
    for(i=0;i<arr_size;i++){
        flags[i]=isPowerOfTwo(arr[i]);
    }
        
    return flags;
}


int main(){



	return 0;
}