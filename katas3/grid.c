#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void countX(int arr_size, char** arr) {

    char *line;

    unsigned long long a,b;

    unsigned long long prevA,prevB;

    prevA=0;
    prevB=0;
   
   	unsigned long long result=0l;

   	int i;

    for(i=0;i<arr_size;i++){
    	line=arr[i];

    	a=atoi(line);

    	while((*line)!=' '){
    		line++;
    	}

    	line++;

    	b=atoi(line);

    	if((prevA==0 && prevB==0)
    		|| (a<=prevA && b<=prevB)){
            
    		result=a*b;

    		prevA=a;
    		prevB=b;
    	}
    	else if (a<=prevA &&  b>prevB){
    		result=a*prevB;
            prevA=a;
    	}
    	else if(a>prevA && b<=prevB){
    		result=prevA*b;
            prevB=b;
    	}
    	else if(a>prevA && b>prevB){
    		result=prevA*prevB;
    	}
    }

    printf("%llu\n", result);
}

int main(){

	char **arr=(char**)calloc(3,sizeof(char*));

	
	arr[0]=strdup("2 3");
	arr[1]=strdup("3 7");
	arr[2]=strdup("4 1");

	countX(3,arr);

	return 0;
}