#include <stdio.h>

long double factorial(int n){
    long double prod=1L;
    int i;
    for(i=1;i<=n;i++){
        prod*=((long double)i);
    }
    return prod;
}

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int k;
    
    scanf("%d",&k);
    
	int cols=1;
	int i,j;

	for(i=0;i<k;i++){
	    long double n=factorial(i);
		for(j=0;j<cols;j++){
			long double r=factorial(j);
			long double n_r=factorial(i-j);

			long double res=(n)/(r*n_r);

			if(res==0){res=1ll;}


			if(j==cols-1){
				printf("%.0Lf\n", res);
			}
			else{
				printf("%.0Lf ", res);
			}
		}
		cols++;
	}

	return 0;
}