/*
Solucion http://acm.tju.edu.cn/toj/showp1218.html
programador: Diego Yanez
*/
#include <stdlib.h>
#include <stdio.h> 

int get_col(int,int**);
int get_row(int,int**);

int main(){	
	int willnumber=0;
	int majacolumn=0,  majarow=0;

	//read input 
	scanf("%d", &willnumber);

	int **resultados=(int**)calloc(100,sizeof(int*));

	int i;

	for(i=0;i<100;i++){
		resultados[i]=(int*)calloc(4,sizeof(int));
	}

	//Process
	while(willnumber > 0){
		//Process
		majacolumn = get_col(willnumber,resultados);
		majarow = get_row(willnumber,resultados);
		//Print result
		printf("%d,%d\n", majacolumn, majarow);
		//read input 
		scanf("%d", &willnumber);
	}
	
	return 0;
}

int get_col(int willnumber,int**resultados){
	int column = 0;
	int currentcelda =2;
	int mincolumn=0, maxcolumn=0;
	int maxCapacity = 0;
	int i;
	if (willnumber == 1){
		return 0;
	}

	if(resultados[willnumber-1][2]){
		//printf("simon\n");
		return resultados[willnumber-1][0];
	}

	while(currentcelda < willnumber){
		mincolumn -= 1;
		maxcolumn +=1;
		maxCapacity +=1;
		//desplaza izquierda
		for(;column > mincolumn; column--, currentcelda++){
			resultados[currentcelda-1][0]=column;
			resultados[currentcelda-1][2]=1;
			if(currentcelda == willnumber){
				return column;
			}
		}
		//hacia arriba
		for(i=0;i<maxCapacity; currentcelda++,i++){
			resultados[currentcelda-1][0]=column;
			resultados[currentcelda-1][2]=1;
			if(currentcelda == willnumber){
				return column;
			}
		}
		//derecha
		for(; column < maxcolumn; column++, currentcelda++){
			resultados[currentcelda-1][0]=column;
			resultados[currentcelda-1][2]=1;
			if(currentcelda == willnumber){
				return column;
			}
		}
		//abajo
		for(i=0;i<maxCapacity+1; currentcelda++,i++){
			resultados[currentcelda-1][0]=column;
			resultados[currentcelda-1][2]=1;
			if(currentcelda == willnumber){
				return column;
			}
		}

	}
	return column;
}

int get_row(int willnumber,int**resultados){
	int row = 1;
	int currentcelda =2;
	int minrow=0, maxrow=0;
	int maxCapacity = 0;
	int i;

	if (willnumber == 1){
		return 0;
	}

	if(resultados[willnumber-1][3]){
		//printf("simon\n");
		return resultados[willnumber-1][1];
	}

	while(currentcelda < willnumber){
		minrow -= 1;
		maxrow +=1;
		maxCapacity += 1;


		for(i=0; i<maxCapacity; currentcelda++,i++){
			resultados[currentcelda-1][1]=row;
			resultados[currentcelda-1][3]=1;
			if(currentcelda == willnumber){
				return row;
			}
		}

		for(;row > minrow; row--, currentcelda++){
			resultados[currentcelda-1][1]=row;
			resultados[currentcelda-1][3]=1;
			if(currentcelda == willnumber){
				return row;
			}
		}

		for(i=0;i<maxCapacity; currentcelda++,i++){
			resultados[currentcelda-1][1]=row;
			resultados[currentcelda-1][3]=1;
			if(currentcelda == willnumber){
				return row;
			}
		}
		
		for(; row < maxrow+1; row++, currentcelda++){
			resultados[currentcelda-1][1]=row;
			resultados[currentcelda-1][3]=1;
			if(currentcelda == willnumber){
				return row;
			}
		}
	}
	return row;
}