#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* isPangram(int strings_size, char**strings){

	char *result=(char*)calloc(strings_size+1,sizeof(char));
	result[strings_size]='\0';
	int *count=(int*)calloc(26,sizeof(int));

	int i, j , length, isThereZero;

	for(i=0;i<strings_size;i++){
		isThereZero=0;
		length=strlen(strings[i]);
		j=0;
		while(j<length){
			if(strings[i][j]!=' '){
				count[((int)strings[i][j])-97]++;
			}
			j++;
		}
		j=0;
		for(;j<26;j++){
			if(count[j]==0){
				isThereZero=1;
				break;
			}
		}
		if(isThereZero){
			result[i]='0';
		}
		else{
			result[i]='1';
		}
		j=0;
		for(;j<26;j++){
			count[j]=0;
		}
	}

	free(count);
	count=0;
	return result;
}

char**read_input_file(char *path, int *size){
	FILE *f=fopen(path,"r");
		char *char_num=(char*)calloc(4,sizeof(char));
		char cur_char;

		int i;

		for(i=0;i<3;i++){
			cur_char=(char)fgetc(f);
			if((int)cur_char==10){
				cur_char=(char)fgetc(f);
				break;
			}
			else{
				char_num[i]=(char)cur_char;
			}
		}

		int strings_size=atoi(char_num);

		char **strings=(char**)calloc(strings_size,sizeof(char*));

		for(i=0;i<strings_size;i++){
			strings[i]=(char*)calloc(100001,sizeof(char));
			strings[i][0]='\0';
		}

		i=0;
		int j=0;
		while(i<strings_size){
			while((int)cur_char!=10 && cur_char!=EOF){
				strings[i][j]=cur_char;
				j++;
				cur_char=fgetc(f);
			}
			i++;
			j=0;
			if(cur_char!=EOF){
				cur_char=fgetc(f);
			}
		}
	

	*size=strings_size;

	return strings;
}


char* get_output_file(char *path,int size){
	FILE *f=fopen(path,"r");
	char cur_char=fgetc(f);
	int i=0;
	char *output=(char*)calloc(size+1,sizeof(char));
	output[size]='\0';
	while(cur_char!=EOF){
		output[i]=cur_char;
		cur_char=fgetc(f);
		i++;
	}
	return output;
}

int main(void){

	char *path_in=strdup("test_casespangrams/input0XX.txt");
	char *path_out=strdup("test_casespangrams/output0XX.txt");

	int i=0;
	while(i<=12){
		if(i<10){
			path_in[25]='0';
			path_in[26]=(char)(48+i);
			path_out[26]='0';
			path_out[27]=(char)(48+i);
		}
		else{
			path_in[25]='1';
			path_out[26]='1';
			if(i==10){
				path_in[26]='0';
				path_out[27]='0';
			}
			else if(i==11){
				path_in[26]='1';
				path_out[27]='1';
			}
			else if(i==12){
				path_in[26]='2';
				path_out[27]='2';
			}
		}
		int strings_size;

		char**strings=read_input_file(path_in,&strings_size);

		char *result=isPangram(strings_size,strings);

		char *correct=get_output_file(path_out,strings_size);

		int correctness=strcmp(correct,result);

		printf("Test case number %d\n", i);

		if(correctness==0){
			printf("success\n");
		}
		else{
			printf("fail\n");
		}

		printf("%s\n", result);
		printf("%s\n\n", correct);

		i++;
	}
	return 0;
}