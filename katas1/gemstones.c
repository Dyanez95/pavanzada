#include <string.h>
#include <stdlib.h>
#include <stdio.h>



int gemstones(int rock_size, char** rocks){

	int *count=(int*)calloc(26,sizeof(int));
	int *changed=(int*)calloc(26,sizeof(int));

	int i;
	int j=0;
	int length;

	for(i=0;i<rock_size;i++){
		length=strlen(rocks[i]);
		while(j<length){
			if(!changed[(int)rocks[i][j]-97]){
				count[(int)rocks[i][j]-97]++;
				changed[(int)rocks[i][j]-97]++;
			}
			j++;
		}
		free(changed);
		changed=(int*)calloc(26,sizeof(int));
		j=0;
	}

	i=0;
	int total_elements=0;

	for(i=0;i<26;i++){
		if(count[i]==rock_size){
			total_elements++;
		}
	}

	return total_elements;
}

int main(void){


	char **words=(char**)calloc(3,sizeof(char*));

	words[0]=strdup("abcdde");
	words[1]=strdup("baccd");
	words[2]=strdup("eeabg");

	int res=gemstones(3,words);

	printf("%d\n", res);

	return 0;
}
