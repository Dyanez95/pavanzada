#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void mystery(int letter_size, char **letter){
	int i,j,mid,length,counter;

	for(i=0;i<letter_size;i++){

		length=strlen(letter[i]);
		counter=0;
		j=0;

		if(length==1){
			printf("%d\n", counter);
		}
		else{
			mid=length/2;

			int left,right;
			while(j<mid){

				left=(int)(letter[i][j]);
				right=(int)(letter[i][length-j-1]);

				int diff=left-right;

				if(diff<0){
					diff*=-1;
				}

				counter+=diff;
				j++;
			}
			printf("%d\n", counter);
		}
	}
}

int main(void){
	char **string=(char**)calloc(4,sizeof(char*));

	string[0]=strdup("abc");
	string[1]=strdup("abcba");
	string[2]=strdup("abcd");
	string[3]=strdup("az");

	mystery(4,string);

	return 0;
}