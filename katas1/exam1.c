#include <stdio.h>
#include <stdlib.h>
#include <math.h>

char*doesCircleExist(char*commands){

	char *left=(char*)strdup("L");
	char *right=(char*)strdup("R");

	char *ans=(char*)calloc(4,sizeof(char));

	int comp=strcmp(commands,left);

	if(comp==0){
		ans=strdup("YES");
		return ans;
	}

	int cmp=strcmp(commands,right);

	if(comp==0){
		ans=strdup("YES");
		return ans;
	}

	int x,y;
	x=0;
	y=0;

	int i;
	int length=(int)strlen(commands);

	int goingLeft,goingRight,goingUp,goingDown;

	goingRight=0;
	goingLeft=0;
	goingUp=1;
	goingDown=0;

	double dis1,dis2;

	for(i=0;i<length*2;i++){
		if(commands[i]=='G'){
			if(goingUp){
				y++;
			}
			else if(goingDown){
				y--;
			}
			else if(goingLeft){
				x--;
			}
			else if(goingRight){
				x++;
			}
		}
		else if(commands[i]=='L'){
			if(goingUp){
				goingLeft=1;
				goingRight=0;
				goingUp=0;
				goingDown=0;
			}
			else if(goingRight){
				goingUp=1;
				goingRight=0;
				goingLeft=0;
				goingDown=0;
			}
			else if(goingLeft){
				goingDown=1;
				goingUp=0;
				goingRight=0;
				goingLeft=0;				
			}
			else if(goingDown){
				goingLeft=1;
				goingUp=0;
				goingRight=0;
				goingDown=0;
			}
		}
		else if(commands[i]=='R'){
			if(goingUp){
				goingRight=1;
				goingUp=0;
				goingLeft=0;
				goingDown=0;
			}
			else if(goingRight){
				goingDown=1;
				goingUp=0;
				goingRight=0;
				goingLeft=0;
			}
			else if(goingLeft){
				goingUp=1;
				goingRight=0;
				goingLeft=0;
				goingDown=0;
			}
			else if(goingDown){
				goingRight=1;
				goingUp=0;
				goingLeft=0;
				goingDown=0;
			}
		}
	}

	if(x==0 && y==0){
		ans=strdup("YES");
		return ans;
	}
	ans=strdup("NO");
	return ans;
}

int main(void){
	int c;
	while(c=getchar()!='\n'){
		putchar(c+('a'-'A'));
	}
	putchar('\n');
	return 0;
}