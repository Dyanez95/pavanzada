#include <stdio.h>
#include <string.h>

void lastLetter(char *word){
	int length=strlen(word);

	if(length>1){
		printf("%c %c\n",word[length-1],word[length-2]);
	}
	else{
		printf("%c\n", word[length-1]);
	}
}

int main(void){

	char *word="APPLE";

	lastLetter(word);

	return 0;
}