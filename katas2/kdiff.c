#include <stdio.h>

int KDifference(int a_size, int* a, int k) {
    int i,j;
    int num_of_diff=0;
    for(i=0;i<a_size-1;i++){
        for(j=i+1;j<a_size;j++){
            int diff=a[i]-a[j];
            if(diff<0){
                diff*=-1;
            }
            if(diff==k){
                num_of_diff++;
            }
        }
    }
    return num_of_diff;
}