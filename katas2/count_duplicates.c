#include <stdio.h>

int countDuplicates(int numbers_size, int* numbers) {

	int *repeated=(int*)calloc(1001,sizeof(int));
	int counter=0;
	int i;

	for(i=0;i<numbers_size;i++){
		repeated[numbers[i]]++;
	}

	for(i=1;i<1001;i++){
		if(repeated[i]>1){
			counter++;
		}
	}
	free(repeated);

	return counter;
}

int main(void){
	return 0;
}