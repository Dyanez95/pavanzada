#include <stdio.h>


long sumOfIntegers(int arr_size, int* arr) {
    
    int i;
    long sum=0l;
    for(i=0;i<arr_size;i++){
        sum+=arr[i];
    }
    return sum;
}

int main(void){

	int a[5]={1,2,3,4,5};

	long res=sumOfIntegers(5,(int*)a);

	printf("%ld\n", res);

	return 0;
}