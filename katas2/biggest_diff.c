#include <stdio.h>

int maxDifference(int a_size, int* a) {
	/*int biggest_diff=-1;
	
	int i,j;

	for(i=0;i<a_size-1;i++){
		for(j=i+1;j<a_size;j++){
			if(a[j]>a[i]){
                if((a[j]-a[i])>biggest_diff){
                    biggest_diff=a[j]-a[i];
                }
            }
		}
	}

	return biggest_diff;*/

	int biggest_diff=-1;
	
	int i,j;
    
    int prev=a[0];

	for(i=0;i<a_size-1;i++){
        if(a[i]>prev){
            //prev=a[i];
            continue;
        }
        else{
            prev=a[i];
        }
		for(j=i+1;j<a_size;j++){
           if((a[j]>a[i]) && ((a[j]-a[i])>biggest_diff)){
              biggest_diff=a[j]-a[i];
           }
		}
	}

	return biggest_diff; 

}

int main(void){
	return 0;
}