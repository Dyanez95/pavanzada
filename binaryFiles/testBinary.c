#include <stdio.h>
#include <assert.h>


typedef struct st{
	char name[10];
	int id;
}student_t;

int main(){

	FILE *fstudent;
	char *fname="students.std";

	assert(((fstudent=fopen(fname,"w"))!=NULL) && "ERROR CANNOT OPEN FILE");

	int i;

	student_t student;

	for(i=0;i<10;i++){
		sprintf(student.name,"juan%2d",i);
		student.id=i;
		fwrite(&student,sizeof(student_t),1,fstudent);
	}
	fclose(fstudent);
	return 0;
}