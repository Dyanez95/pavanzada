#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

typedef struct st{
	char name[10];
	int id;
}student_t;

int main(){

	int i;
	int fd;
	char *fname="students.std";

	student_t *students;
	
	assert(((fd=open(fname,O_RDONLY)) !=0) && "ERROR CANNOT OPEN FILE");

	students=mmap(NULL,10*sizeof(student_t),PROT_READ | PROT_WRITE,MAP_PRIVATE,fd,0);

	for(i=0;i<10;i++){
		printf("st.name = %s st.id = %d\n", students[i].name,students[i].id);
		students[i].id=20;
	}

	munmap(students,10*sizeof(student_t));

	close(fd);

	return 0;
}