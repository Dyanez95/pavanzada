#include <stdio.h>
#include <assert.h>


typedef struct st{
	char name[10];
	int id;
}student_t;

int main(){

	FILE *fstudent;
	char *fname="students.std";

	assert(((fstudent=fopen(fname,"r"))!=NULL) && "ERROR CANNOT OPEN FILE");

	int i;

	student_t student;

	fseek(fstudent,5*sizeof(student_t),SEEK_SET);

	fread(&student,sizeof(student_t),1,fstudent);
	printf("st.name = %s ; st.id =%d\n", student.name,student.id);

	/*while(fread(&student,sizeof(student_t),1,fstudent)>0){
		printf("st.name = %s ; st.id = %d\n",student.name,student.id);
	}*/

	fclose(fstudent);
	return 0;
}