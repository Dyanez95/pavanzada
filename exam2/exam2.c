#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void squares(int size, char **nums){
	int i,j,num_squares;

	char *holder;

	for(i=0;i<size;i++){

		unsigned int low,high;

		holder=(char*)calloc(11,sizeof(char));

		char cur_char=nums[i][0];

		holder[0]=cur_char;

		cur_char=nums[i][1];

		j=1;

		while(cur_char!=' '){
			holder[j]=cur_char;
			j++;
			cur_char=nums[i][j];
		}

		low=atoi(holder);

		free(holder);

		holder=NULL;

		holder=(char*)calloc(11,sizeof(char));

		cur_char=nums[i][j+1];

		holder[0]=cur_char;

		cur_char=nums[i][j+2];

		int k=1;

		j+=2;

		while(cur_char!='\n'){
			holder[k]=cur_char;
			k++;
			j++;
			cur_char=nums[i][j];
		}

		high=atoi(holder);

		free(holder);

		num_squares=0;
		for(j=low;j<=high;j++){
			double x=sqrt((double)j);
			if(x==floor(x)){
				num_squares++;
			}
		}
		printf("%d\n", num_squares);
		num_squares=0;
		j=0;
	}
}

void squares2(int size, char **nums){
	int low,high,i;

	char *line;

	int num_squares;

	for(i=0;i<size;i++){

		line=nums[i];

		low=atoi(line);

		while(*line!=' '){line++;}

		line++;

		high=atoi(line);

		num_squares=0;

		int s_low=(int)sqrt((double)low);
		int s_high=(int)sqrt((double)high);

		int j;

		for(j=s_low;j<=s_high;j++){
			if((j*j)<=high && (j*j)>=low){
				num_squares++;
			}
		}
		printf("%d\n", num_squares);
	}
}

int main(void){


	char **array=(char**)calloc(2,sizeof(char*));

	array[0]=strdup("5 16\n\0");
	array[1]=strdup("27 31\n\0");

	squares2(2,array);


	return 0;
}