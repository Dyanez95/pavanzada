#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "hashset.h"

#include "bool.h"

bool debug=true;

typedef struct {
	char *key;
	vector *value;
}hashelem_t;

typedef struct {
	char c;
	int frequency;
}  vecelem_t;

int hash_word(const void *p, int numBuckets){
	long kHashMultiplier = -1664117991L;          

	hashelem_t *he=(hashelem_t*)p;

	char *s=he->key;

	int i;
	unsigned long hashcode = 0;
  
	for (i = 0; i < strlen(s); i++){  
		hashcode = hashcode * kHashMultiplier + tolower(s[i]);  
  	}
	return (int)(hashcode % numBuckets);                                
}

void printVectorElem(void *elem, void *fp){
	vecelem_t *freq=(vecelem_t*)elem;
	fprintf((FILE*)fp, "<<%c : %d \n>>",freq->c,freq->frequency);
}

void printHashElem(void *elem, void *fp){
	hashelem_t *h=(hashelem_t*)elem;
	fprintf((FILE*)fp, "<<seed %s :\n>>", h->key);
	VectorMap(h->value,printVectorElem,fp);
}

int compare_word(const void *p1, const void *p2){
	hashelem_t *he1=(hashelem_t*)p1;
	hashelem_t *he2=(hashelem_t*)p2;
	return strcmp(he1->key,he2->key);
}

void free_word(void *p){
	hashelem_t *he=(hashelem_t*)p;
	free(he->key);
	VectorDispose(he->value);
}

int compare_char(const void *p1, const void *p2){
	vecelem_t *v1=(vecelem_t*)p1;
	vecelem_t *v2=(vecelem_t*)p2;

	return (v1->c-v2->c);
}

void usage(char *s);
void groupWordsBy(FILE *f, int k, char *word, char *next);
void getSeeds(char *fname, int k, hashset *seeds);

int main(int argc, char **argv){

	if(argc!=4){
		usage(argv[0]);
		exit(-1);
	}

	int k;
	char *nombre;
	int w;

	hashset seeds;

	k=atoi(argv[1]);

	nombre=strdup(argv[2]);
	w=atoi(argv[3]);
	

	if(debug)printf("<< parametros %d %s %d >>\n",k,nombre,w);

	HashSetNew(&seeds,sizeof(hashelem_t),1000,hash_word,compare_word,free_word);

	getSeeds(nombre,k,&seeds);

	HashSetMap(&seeds,printHashElem,stdout);

	free(nombre);

	HashSetDispose(&seeds);

	return 0;
}

void usage(char *s){
	printf("Error: incorrect number of arguments\n"
		"Usage %s k name w:\n"
		"Donde: \n"
			"\t k: number of letters to group\n"
			"\t name: name or path of the text file\n"
			"\t w: number of words to produce\n",s);
}

void groupWordsBy(FILE *fp, int k, char *word, char *next){
	int i;

	long file_pos;

	file_pos=ftell(fp);

	for(i=0;i<k;i++){
		if((word[i]=fgetc(fp))==EOF){
			break;
		}
	}
	word[i]='\0';

	if(!feof(fp)){
		*next=fgetc(fp);
		fseek(fp,file_pos+1,SEEK_SET);
	}
}

void getSeeds(char *fname, int k, hashset *seeds){

	char *word;
	char next;
	FILE *fp;

	hashelem_t hlocal;
	hashelem_t *found;

	int vfound;

	vecelem_t vlocal;

	vecelem_t *elemFound;

	fp=fopen(fname,"r");

	assert(fp!=NULL && "opening file failed");

	word=(char*)malloc(sizeof(char)*(k+1));

	while(!feof(fp)){
		groupWordsBy(fp,k,word,&next);
		if(next==EOF)break;
		if(strlen(word)<k)break;
		if(debug)printf("<< %s : %c >>\n", word,next);

		hlocal.key=word;

		if((found=(hashelem_t*)HashSetLookup(seeds,&hlocal))!=NULL){

			vlocal.c=next;

			vfound=VectorSearch(found->value,&vlocal,compare_char,0,false);

			if(vfound>=0){
				elemFound=(vecelem_t*)VectorNth(found->value,vfound);
				elemFound->frequency+=1;
			}
			else{
				elemFound=(vecelem_t*)malloc(sizeof(vecelem_t));
				elemFound->c=next;
				elemFound->frequency=1;
				VectorAppend(found->value,elemFound);
			}


		}
		else{
			hlocal.key=strdup(word);
			hlocal.value=(vector*)malloc(sizeof(vector));
			VectorNew(hlocal.value,sizeof(vecelem_t),NULL,4);

			elemFound=(vecelem_t*)malloc(sizeof(vecelem_t));
			elemFound->c=next;
			elemFound->frequency=1;

			VectorAppend(hlocal.value,elemFound);

			HashSetEnter(seeds,&hlocal);
		}
	}

	free(word);
	fclose(fp);
}