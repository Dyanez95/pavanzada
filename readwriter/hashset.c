#include "hashset.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>

void HashSetNew(hashset *h, int elemSize, int numBuckets,
		HashSetHashFunction hashfn, HashSetCompareFunction comparefn, HashSetFreeFunction freefn)
{
	h->buckets=(vector*)malloc(sizeof(vector)*numBuckets);

	assert(elemSize>0 && "invalid elemsize");
	assert(numBuckets>0 && "invalid numBuckets");
	assert(hashfn!=NULL && comparefn!=NULL && "invalid functions");


	assert(h->buckets!=NULL && "allocation failed");

	h->size=numBuckets;
	h->elemSize=elemSize;
	h->hashfn=hashfn;
	h->comparefn=comparefn;
	h->freefn=freefn;

	int i;

	for(i=0;i<numBuckets;i++){
		VectorNew(h->buckets+i,elemSize,freefn,1);
	}
}

void HashSetDispose(hashset *h)
{
	int i;

	for(i=0;i<h->size;i++){
		VectorDispose(h->buckets+i);
	}
	free(h->buckets);
}

int HashSetCount(const hashset *h)
{ 
	int count=0;
	int i;

	for(i=0;i<h->size;i++){
		count+=VectorLength(h->buckets+i);
	}

	return count; 
}

void HashSetMap(hashset *h, HashSetMapFunction mapfn, void *auxData)
{

	assert(mapfn!=NULL && "invalid function pointer");

	int i;

	for(i=0;i<h->size;i++){
		VectorMap(h->buckets+i,mapfn,auxData);
	}

}

void HashSetEnter(hashset *h, const void *elemAddr)
{
	assert(elemAddr!=NULL && "invalid elemAddr");

	int hash=h->hashfn(elemAddr,h->size);

	//int pos_search=VectorSearch(h->buckets+hash,elemAddr,h->comparefn,0,0);

	/*if(VectorLength(h->buckets+hash)==0){
		VectorAppend(h->buckets+hash,elemAddr);
	}
	else{*/

	int pos_search=VectorSearch(h->buckets+hash,elemAddr,h->comparefn,0,0);
	if(pos_search<0){
		VectorAppend(h->buckets+hash,elemAddr);
	}
	else{
		VectorReplace(h->buckets+hash,elemAddr,pos_search);
	}		
}

void *HashSetLookup(const hashset *h, const void *elemAddr)
{ 

	assert(elemAddr!=NULL && "invalid elemAddr");

	int hash=h->hashfn(elemAddr,h->size);

	/*int pos=VectorSearch(h->buckets+hash,elemAddr,h->comparefn,0,0);

	if(pos<0){
		return NULL;
	}

	return VectorNth(h->buckets+hash,pos);*/

	void *result=NULL;

	//if(VectorLength(h->buckets+hash)>0){
	int pos=VectorSearch(h->buckets+hash,elemAddr,h->comparefn,0,0);

	if(pos>=0){
		result=VectorNth(h->buckets+hash,pos);
	}
	//}

	return result;
}
