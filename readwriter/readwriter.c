#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "hashset.h"
#include <time.h>


//hashset of all HashSetWord's
static hashset all_tokens;

//struct to hold a character and how often it follows a word
typedef struct {
	char letter;
	int freq;
} Frequency;

//struct to hold a word and a hashset of all characters that follow it in a text file
typedef struct {

	char *word;
	hashset *next_chars;

} HashSetWord;

//pointers to Frequency should be initialized with malloc
void free_frequency(void *p){
	Frequency **f=(Frequency**)p;
	free(*f);
}

int hash_frequency(const void *p, int numBuckets){
	Frequency **f=(Frequency**)p;
	return ((int)(*f)->letter)%numBuckets;
}

int compare_frequency(const void *p1, const void *p2){
	Frequency **f1=(Frequency**)p1;
	Frequency **f2=(Frequency**)p2;
	return (int)((*f1)->letter-(*f2)->letter);
}

//constructs a HashSetWord object, hs should be allocated with malloc before calling this method
void newHashSetWord(HashSetWord *hs,const char *word){
	hs->word=(char*)calloc(strlen(word)+1,sizeof(char));
	memmove(hs->word,word,sizeof(char)*(strlen(word)+1));
	hs->next_chars=(hashset*)malloc(sizeof(hashset));	
	HashSetNew(hs->next_chars,sizeof(Frequency*),50,hash_frequency,compare_frequency,free_frequency);
}

void free_hashset_word(void *p){
	HashSetWord **hs=(HashSetWord**)p;
	free((*hs)->word);
	HashSetDispose((*hs)->next_chars);
	free((*hs)->next_chars);
	free(*hs);
}

//hashset by the string contained in hs
int hash_hashset_word(const void *p, int numBuckets){
	long kHashMultiplier = -1664117991L;          

	HashSetWord **hs=(HashSetWord**)p;

	int i;
	unsigned long hashcode = 0;
  
	for (i = 0; i < strlen((*hs)->word); i++){  
		hashcode = hashcode * kHashMultiplier + tolower((*hs)->word[i]);  
  	}
	return (int)(hashcode % numBuckets);                                
}

//compares by strings contained in hashsets
int compare_hashset_word(const void *p1, const void *p2){
	HashSetWord **hs1=(HashSetWord**)p1;
	HashSetWord **hs2=(HashSetWord**)p2;
	return strcmp((*hs1)->word,(*hs2)->word);
}

//used to iterate over a Hashset of frequencies and obtain the total frequency
//result stored in result
void obtain_freq(void *p1, void *p2){
	Frequency **f=(Frequency**)p1;
	int *result=(int*)p2;
	(*result)+=(*f)->freq;
}

//used to iterate over a Hashset of frequencies and obtain maximum frequency
//result stored in result
void obtain_max_freq(void *p1, void *p2){
	Frequency **f=(Frequency**)p1;
	int *max=(int*)p2;
	if(((*f)->freq)>(*max)){
		*max=(*f)->freq;
	}
}

/*
Function to obtain the character which its relative frequency is closest to a random number
Used to select next character in randomly selected word
Workaround the fact that frequencies are unordered

container[0]:accumulated frequency in hashset of frequencies
container[1]:random number in interval [0,100]
container[2]:relative frequency of previous possible candidate to be selected in interval [0,100], 
or -1 if there's no previous candidate

container[3]:character stored in candidate frequency, casted to integer

container[4]: maximum frequency found in current hashset of frequencies
*/
void obtain_random_selected(void *p1, void *p2){
	Frequency **f=(Frequency**)p1;
	int *container=(int*)p2;

	//calculate relative frequency, using integers instead of doubles
	//multiply by 100 first to ensure integer division doesnt truncate the number down to zero
	int rel_freq=((*f)->freq*100)/container[0];

	//happens when the random number is greater than the greatest relative frequency
	//found in the hashset
	//choose character with largest frequency
	if((*f)->freq>=container[4] && container[1]>rel_freq){
		
		container[2]=rel_freq;
		container[3]=(int)((*f)->letter);
	}
	//check if random number is less than the relative frequency of current Frequecy
	else if(container[1]<=rel_freq){
		//check if there has been a previous candidate to been the chosen frequency
		//if there's no previous candidate, the current frequency is the new candidate
		if(container[2]<0){
			container[2]=rel_freq;
			container[3]=(int)((*f)->letter);
		}
		//if there's a previous candidate, check the difference between their relative frequencies
		//and the random number, select current frequency if its difference is smaller
		else{
			int diff1=rel_freq-container[1];
			int diff2=container[2]-container[1];

			if(diff1<diff2){
				container[2]=rel_freq;
				container[3]=(int)((*f)->letter);
			}
		}
	}
}

//lazy way to get new random word if we get stuck generating random words
void select_new_random_word(void *p1, void *p2){
	HashSetWord **hs=(HashSetWord**)p1;
	char *word=(char*)p2;

	int r=rand()%2;

	if(r){
		memmove(word,(*hs)->word,strlen((*hs)->word)*sizeof(char));
	}
}

//count characters that we consider blank to determine how many words have been produced
int count_blanks(char *s){
	int i;
	int count=0;
	for(i=0;i<strlen(s);i++){
		if(s[i]==' ' || s[i]=='\n' || s[i]=='\t'){
			count++;
		}
	}
	return count;
}

//read input file, generates hashsets of words and frequencies and stores them in the arr_tokens hashset
void read_file(unsigned int token_size, char *file_name, char *first_word){

	FILE *f=fopen(file_name,"r");

	assert(f!=NULL && "opening file failed");

	char cur_char;

	char *cur_word=(char*)calloc(token_size+1,sizeof(char));

	int i;

	//copy token_size-1 characters into cur_word
	for(i=0;i<token_size;i++){
		cur_char=fgetc(f);

		/*if(cur_char=='\n' || cur_char=='\t' || cur_char=='\r'){
			cur_char=' ';
		}*/

		cur_word[i]=cur_char;
	}

	//first word found on file will be used to generate random words
	memmove(first_word,cur_word,sizeof(char)*(token_size+1));

	//the actual stop condition is located inside the loop
	while(true){

		//create new HashsetWOrd and intialize it

		HashSetWord *new_hs=(HashSetWord*)malloc(sizeof(HashSetWord));

		newHashSetWord(new_hs,cur_word);

		//check if current word is already in the all_tokens hashset

		HashSetWord **find_hs=(HashSetWord**)HashSetLookup(&all_tokens,&new_hs);

		//if we find out that the current word is already in the hashset, its easier to
		//just update the one already there, rather than attempting to update its hashset
		//or attempting to join its hashset with the one produced here
		if(find_hs!=NULL){
			free_hashset_word(&new_hs);
			//use hashset found instead of new one
			new_hs=(*find_hs);
		}

		//cur char now holds the character following the current word
		cur_char=fgetc(f);

		//we've reached the end of the file, stop loop
		if(cur_char==EOF || cur_char<0){
			//free_hashset_word(&new_hs);
			if(find_hs==NULL){
				free_hashset_word(&new_hs);
			}
			break;
		}

		/*if(cur_char=='\n' || cur_char=='\t'){
			cur_char=' ';
		}*/

		//create new Frequency with current character and frequency of 1
		Frequency *fq=(Frequency*)malloc(sizeof(Frequency));

		fq->letter=cur_char;
		fq->freq=1;

		//search for current character in the current hashsetword
		Frequency **res=(Frequency**)HashSetLookup(new_hs->next_chars,&fq);

		//not found, simply insert the new frequency
		if(res==NULL){
			HashSetEnter(new_hs->next_chars,&fq);
		}
		//found same character, update frequency and insert again
		else{
			fq->freq+=((*res)->freq);
			HashSetEnter(new_hs->next_chars,&fq);
		}

		//move characters in current word one space to the left, deleting the first character
		for(i=1;i<token_size;i++){
			cur_word[i-1]=cur_word[i];
		}	

		//next word will be the current word shifted to the left and added the current char
		cur_word[token_size-1]=cur_char;

		//if didnt find current word in all_tokens, new hashsetword must be inserted in it
		if(find_hs==NULL){
			HashSetEnter(&all_tokens,&new_hs);
		}
	}

	free(cur_word);

	fclose(f);
}


//generates random words based on the frequencies
//outputs texts into the specified file
//must generate at least max_words random words
//uses first word to start off
void generate_words(char *file_name,unsigned int max_words, char *first_word){
	FILE *f=fopen(file_name,"w");

	assert(f!=NULL && "opening file to write failed");

	int count=0;

	//copy first_word into current word
	char *cur_word=(char*)calloc(strlen(first_word)+2,sizeof(char));
	memmove(cur_word,first_word,sizeof(char)*(strlen(first_word)+1));

	//will be generating random numbers, set seed
	srand((unsigned)time(NULL));
	
	//dummy hashsetword to make searches
	HashSetWord *dummy;

	while(count<=max_words){
		int total_freq=0;

		dummy=(HashSetWord*)malloc(sizeof(HashSetWord));
		newHashSetWord(dummy,cur_word);

		//search for current word in the all_tokens hashset
		//should find something most of the time
		HashSetWord **find=(HashSetWord**)HashSetLookup(&all_tokens,&dummy);

		if(find!=NULL){

			//sentinel value, its changed by the obtain_max_freq
			int max_freq=-1;

			//obtain total frequency and maximum frequency
			HashSetMap((*find)->next_chars,obtain_freq,&total_freq);
			HashSetMap((*find)->next_chars,obtain_max_freq,&max_freq);

			//lazy way to choose a number in interval [0,100]
			int random_select=rand()%101;

			//see obtained_random_selected to know what these assignments mean
			int *container=(int*)calloc(5,sizeof(int));

			container[0]=total_freq;
			container[1]=random_select;
			container[2]=-1;
			container[4]=max_freq;

			//obtain randomly selected character
			HashSetMap((*find)->next_chars,obtain_random_selected,container);

			char *placeholder=(char*)calloc(2,sizeof(char));
			placeholder[0]=(char)container[3];

			//concatenate random character with current word
			strcat(cur_word,placeholder);

			//count how many blank characters are in current word, to know how many 
			//words we have generated
			int count_b=count_blanks(cur_word);

			//eventually this will stop the loop
			count+=count_b;

			//print current word in file
			fputs(cur_word,f);

			int j;

			//shift current word one character to the left
			for(j=1;j<strlen(cur_word);j++){
				cur_word[j-1]=cur_word[j];	
			}
			cur_word[strlen(cur_word)-1]='\0';
			free(placeholder);
			free(container);
		}
		else{
			//didnt find word, choose one randomly that is guaranteed to be in the all_tokens hashset
			HashSetMap(&all_tokens,select_new_random_word,cur_word);

		}
		free_hashset_word(&dummy);
		dummy=NULL;
	}

	fclose(f);
}

/*
	Arguments used:
	argv[1]: token size, should be positive
	argv[2]: name of input file
	argv[3]: maximum number of words to be generated, should be positive
	argv[4]: name of output file, this file does not need to exist, 
		but if it exists, it must be available to write operations

*/
int main(int argc, char **argv){

	assert(argc==5 && "invalid arguments");

	HashSetNew(&all_tokens,sizeof(HashSetWord*),100,hash_hashset_word,compare_hashset_word,free_hashset_word);

	unsigned int token_size=atoi(argv[1]);

	unsigned int max_words=atoi(argv[3]);

	char *first_word=(char*)calloc(token_size+1,sizeof(char));

	read_file(token_size,argv[2],first_word);

	generate_words(argv[4],max_words,first_word);

	return 0;
}