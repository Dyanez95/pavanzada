#include "vector.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <search.h>

void VectorNew(vector *v, int elemSize, VectorFreeFunction freeFn, int initialAllocation)
{

	assert(elemSize>0 && "invalid elemSize");

	assert(initialAllocation>=0 && "invalid initialAllocation");

	v->elems=malloc(elemSize*initialAllocation);

	assert(v->elems!=NULL && "No hay memoria\n");

	v->elemSize=elemSize;

	v->freeFn=freeFn;

	v->initialSize=initialAllocation;

	v->currentPosition=0;
}

void VectorDispose(vector *v)
{
	if(v->freeFn!=NULL){
		void *elemAddress;
		elemAddress=v->elems;

		int i;

		for(i=0;i<v->currentPosition;i++){
			elemAddress=(void*)((char*)v->elems+i*v->elemSize);
			v->freeFn(elemAddress);
		}

	}
	free(v->elems);
}

int VectorLength(const vector *v)
{ 
	return v->currentPosition;
}

void *VectorNth(const vector *v, int position)
{ 

	assert((position<(v->currentPosition) && position>=0)&& "invalid position\n");

	return (void*)((char*)v->elems+position*v->elemSize);
}

void VectorReplace(vector *v, const void *elemAddr, int position)
{
	assert((position<(v->currentPosition) && position>=0)&& "invalid position\n");

	void *cur_add=(void*)((char*)v->elems+position*v->elemSize);
	memcpy(cur_add,elemAddr,v->elemSize);
}

void VectorInsert(vector *v, const void *elemAddr, int position)
{
	assert(position<=(v->currentPosition) && position>=0 && "invalid position\n");

	if(v->currentPosition==v->initialSize){
		if(v->initialSize==0){
			v->initialSize++;
		}
		else{
			v->initialSize*=2;	
		}
		
		v->elems=realloc(v->elems,v->initialSize*v->elemSize);
		assert(v->elems!=NULL && "insert failed\n");
	}

	void *addr1=(void*)((char*)v->elems+position*v->elemSize);

	void *addr2=(void*)((char*)addr1+v->elemSize);

	int size=(v->currentPosition-position)*v->elemSize;

	memmove(addr2,addr1,size);
	memcpy(addr1,elemAddr,v->elemSize);

	v->currentPosition++;
}

void VectorAppend(vector *v, const void *elemAddr)
{
	if(v->currentPosition==v->initialSize){
		if(v->initialSize==0){
			v->initialSize++;
		}
		else{
			v->initialSize*=2;	
		}
		v->elems=realloc(v->elems,v->elemSize*v->initialSize);
		assert(v->elems!=NULL && "append failed\n");
	}
	void *cur_addr=(void*)((char*)v->elems+v->currentPosition*v->elemSize);
	memcpy(cur_addr,elemAddr,v->elemSize);
	v->currentPosition++;
}

void VectorDelete(vector *v, int position)
{
	assert(position<(v->currentPosition) && position>=0 && "invalid position\n");

	void *addr1=(void*)((char*)v->elems+position*v->elemSize);
	void *addr2=(void*)((char*)addr1+v->elemSize);

	int size=(v->currentPosition-(position+1))*v->elemSize;

	memmove(addr1,addr2,size);
	v->currentPosition--;
}

void VectorSort(vector *v, VectorCompareFunction compare)
{
	assert(compare!=NULL && "invalid compare function pointer");
	qsort(v->elems,v->currentPosition,v->elemSize,compare);	
}

void VectorMap(vector *v, VectorMapFunction mapFn, void *auxData)
{

	assert(mapFn!=NULL && "invalid map function");

	int i;
	void *elemAddr;

	for(i=0;i<(v->currentPosition);i++){
		elemAddr=(void*)((char*)v->elems+i*v->elemSize);

		mapFn(elemAddr,auxData);
	}
}

static const int kNotFound = -1;
int VectorSearch(const vector *v, const void *key, VectorCompareFunction searchFn, int startIndex, bool isSorted)
{ 

	//lfind()



	assert(startIndex<=(v->currentPosition) && "invalid position");

	int index=kNotFound;

	if(v->currentPosition==0){
		return index;
	}

	void *elemAddr;

	void *base=(void*)((char*)v->elems+startIndex*v->elemSize);

	size_t nmemb=(size_t)(v->currentPosition-startIndex);


	if(isSorted){
		elemAddr=bsearch(key,base,nmemb,v->elemSize,searchFn);
	}
	else{
		elemAddr=lfind(key,base,&nmemb,v->elemSize,searchFn);
	}

	if(elemAddr!=NULL){
		index=((char*)elemAddr-(char*)v->elems)/(v->elemSize);
	}

	return index; 
} 
