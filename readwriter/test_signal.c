#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

void signalhandled(int s){
	printf("hohoho\n");
}

int main(void){
	signal(10,signalhandled);
	signal(2,signalhandled);
	for(;;){
		printf("esperando por una signal...\n");

		sleep(1);
	}

	return 0;
}