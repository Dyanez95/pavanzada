#include "swap.h"
#include <stdio.h>
#include <string.h>

int test_int(void);
int test_g(void);
int test_array(void);
int test_string(void);

int main(void){



	//test_g();

	//test_array();

	test_string();

	return 0;
}

int test_g(void){
	int a=1;
	int b=3;
	printf("a=%d ",a);
	printf("b=%d\n",b);
	g_swap((void*)&a,(void*)&b,sizeof(int));
	printf("a=%d ", a);
	printf(" b=%d\n", b);
	return 0;
}

int test_array(void){

	int a[]={5,2,3,4};
	int b[]={1,1,1,1};

	int i;
	for(i=0;i<4;i++){
		printf("a[%d]=%d, b[%d]=%d\n",i,a[i],i,b[i]);
	}	

	g_swap(a,b,4*sizeof(int));

	printf("\n");

	for(i=0;i<4;i++){
		printf("a[%d]=%d, b[%d]=%d\n",i,a[i],i,b[i]);
	}
	return 0;
}

int test_string(void){
	char *juan=strdup("Juan");
	char *maria=strdup("ULTRAPODEROSOMEGAMASTER");

	printf("%s es juan %s es maria\n", juan, maria);
	//intercambiar los pointers
	g_swap((void*)&juan,(void*)&maria,sizeof(char*));

	printf("%s es juan %s es maria\n", juan,maria);
	return 0;
}

int test_int(void){
	int a=4;
	int b=5;
	printf("a=%d,",a);
	printf("b=%d\n", b);
	swap(&a,&b);
	printf("a=%d,",a);
	printf("b=%d\n", b);
	return 0;
}