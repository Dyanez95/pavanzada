#include "swap.h"
#include <string.h>


void swap(int *ptrA, int*ptrB){
	int tmp;
	tmp=*ptrA;
	*ptrA=*ptrB;
	*ptrB=tmp;
}

void g_swap(void *ptrA, void *ptrB, int size){

	char tmp[size];

	memcpy(tmp,ptrA,size);
	memcpy(ptrA,ptrB,size);
	memcpy(ptrB,tmp,size);

}