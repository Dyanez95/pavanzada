/*

Problem: http://acm.tju.edu.cn/toj/showp1090.html
Author: Diego Yanez Llamas

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//wall is a matrix of ones and zeroes
//rows and columns describe the dimensions wall
void cityhall(int **wall, int rows, int columns){
	//bricks[i] denotes the amount of bricks of height i needed to repair the wall
	int *bricks=(int*)calloc(rows+1,sizeof(int));
	int i;
	//check each column
	for(i=0;i<columns;i++){
		int j=0;
		//keeps the heigh needed to fill at a certain part of the wall
		int counter=0;
		// check each element of each column
		while(j<rows){
			//if a zero is seen, more height is needed to repair the wall
			if(wall[j][i]==0){
				counter++;
			}
			//first condition means we found a one, a hole in the wall stops. Second condition means we reached the bottom of the wall
			//on both cases we update bricks and counter
			if(wall[j][i]==1 || j==rows-1){
				bricks[counter]++;
				counter=0;
			}	
			j++;
		}
	}
	//display solution, with format: height of brick and amount of bricks of that height needed
	for(i=1;i<=rows;i++){
		if(bricks[i]!=0){
			printf("%d %d\n", i, bricks[i]);
		}
	}
}

void print_matrix(int **m,int rows, int cols){
	int i,j;
	for(i=0;i<rows;i++){
		for(j=0;j<cols;j++){
			printf("[%d] ", m[i][j]);
		}
		printf("\n");
	}
}

int main(void){

	FILE *f=fopen("test.txt","r");

	char *nums=(char*)calloc(3,sizeof(char));

	nums[0]=fgetc(f);

	int rows=atoi(nums);

	fgetc(f);

	nums[0]=fgetc(f);
	nums[1]=fgetc(f);

	int columns=atoi(nums);

	char cur_char;

	fgetc(f);

	int i,j;

	int **wall=(int**)calloc(rows,sizeof(int*));

	for(i=0;i<rows;i++){
		wall[i]=(int*)calloc(columns,sizeof(int));
		for(j=0;j<columns;j++){
			cur_char=fgetc(f);
			wall[i][j]=(int)((int)cur_char-48);
		}
		fgetc(f);
	}

	print_matrix(wall,rows,columns);
	printf("\n");
	cityhall(wall,rows,columns);

	return 0;
}